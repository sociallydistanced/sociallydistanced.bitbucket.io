// external js: isotope.pkgd.js, imagesloaded.pkgd.js
console.log('It has loaded');
// init Isotope
var $grid = $('.grid').isotope({
  itemSelector: '.grid-item',
  percentPosition: true,
  masonry: {
    columnWidth: '.grid-sizer'
  }
});
// layout Isotope after each image loads
$grid.imagesLoaded().progress( function() {
  $grid.isotope('layout');
});  

$('input').change(function(){
  // Get the value of the group name='option'
  var selectedValue = $("input[name='who']:checked").val();
  // Hide all
  $('.option').hide();
  // Use the option value as a class name to show the selected
  $('.'+selectedValue).show();
});